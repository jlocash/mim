package minecraft

import "log"

var (
	LIBRARY_DOWNLOAD_URL = "https://libraries.minecraft.net/"
	ERR_NOT_DIRECTORY    = "%s is not a directory"
)

type LibraryDownload struct {
	Artifact    FileDownloadInfo            `json:"artifact"`
	Classifiers map[string]FileDownloadInfo `json:"classifiers"`
}

type Library struct {
	Name      string              `json:"name"`
	Rules     []Rule              `json:"rules"`
	Extract   map[string][]string `json:"extract"`
	Natives   map[string]string   `json:"natives"`
	Downloads LibraryDownload     `json:"downloads"`
}

func (l *Library) Compatible() bool {
	os := "linux"
	if len(l.Rules) > 0 {
		for _, rule := range l.Rules {
			if rule.Action == "allow" {
				continue
			}

			for _, o := range rule.OS {
				if o == os {
					return false
				}
			}
		}
	}

	return true
}

func (l *Library) Update(dir string) error {
	if !l.Compatible() {
		return nil
	}

	platform := "linux"

	artifact := l.Downloads.Artifact
	if exists := artifact.Check(dir); !exists {
		if err := artifact.Download(dir); err != nil {
			return err
		}
	}

	if err := artifact.Verify(dir); err != nil {
		log.Println(err.Error())
		if err := artifact.Download(dir); err != nil {
			return err
		}
	}

	if l.Natives == nil {
		return nil
	}

	if _, ok := l.Natives[platform]; !ok {
		return nil
	}

	natives := l.Natives[platform]
	classifier := l.Downloads.Classifiers[natives]

	if exists := classifier.Check(dir); !exists {
		return classifier.Download(dir)
	}

	if err := classifier.Verify(dir); err != nil {
		log.Println(err.Error())
		return classifier.Download(dir)
	}

	return nil
}
