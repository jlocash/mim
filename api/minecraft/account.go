package minecraft

import (
	"encoding/json"
	"fmt"

	"github.com/google/uuid"
)

const AUTH_URL = "https://authserver.mojang.com"

type UserAccount struct {
	Username          string    `json:"username"`
	AccessToken       string    `json:"accessToken"`
	ClientToken       string    `json:"clientToken"`
	AvailableProfiles []Profile `json:"availableProfiles"`
	SelectedProfile   Profile   `json:"selectedProfile"`
	User              *UserInfo `json:"user"`
}

func Authenticate(username string, password string) (UserAccount, error) {
	user := UserAccount{
		Username: username,
	}

	token := uuid.New()
	p := payload{
		Agent: agent{
			Name:    "Minecraft",
			Version: 1,
		},
		Username:    username,
		Password:    password,
		ClientToken: token.String(),
		RequestUser: false,
	}

	url := fmt.Sprintf("%s/%s", AUTH_URL, "authenticate")
	b, err := p.Send(url)
	if err != nil {
		return user, err
	}

	err = json.Unmarshal(b, &user)
	return user, err
}

func (u *UserAccount) Refresh() error {
	p := payload{
		AccessToken: u.AccessToken,
		ClientToken: u.ClientToken,
		RequestUser: false,
	}

	url := fmt.Sprintf("%s/%s", AUTH_URL, "refresh")
	b, err := p.Send(url)
	if err != nil {
		return err
	}

	var aux struct {
		AccessToken string `json:"accessToken"`
		ClientToken string `json:"clientToken"`
	}

	if err := json.Unmarshal(b, &aux); err != nil {
		return err
	}

	if u.ClientToken != aux.ClientToken {
		return fmt.Errorf("token refresh failed (clientToken mismatch)")
	}

	u.AccessToken = aux.AccessToken
	return nil
}

func (u *UserAccount) Validate() (bool, error) {
	p := payload{
		AccessToken: u.AccessToken,
		ClientToken: u.ClientToken,
	}

	url := fmt.Sprintf("%s/%s", AUTH_URL, "validate")
	b, err := p.Send(url)
	if err != nil {
		return false, err
	}

	return len(b) == 0, nil
}

func (u *UserAccount) Invalidate() error {
	p := payload{
		AccessToken: u.AccessToken,
		ClientToken: u.ClientToken,
	}

	url := fmt.Sprintf("%s/%s", AUTH_URL, "invalidate")
	b, err := p.Send(url)
	if err != nil {
		return err
	}

	if len(b) != 0 {
		return fmt.Errorf("failed to invalidate user account %s", u.Username)
	}

	return nil
}
