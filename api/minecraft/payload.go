package minecraft

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type agent struct {
	Name    string `json:"name"`
	Version int    `json:"version"`
}

type payload struct {
	AccessToken string `json:"accessToken,omitempty"`
	Agent       agent  `json:"agent,omitempty"`
	ClientToken string `json:"clientToken"`
	Username    string `json:"username,omitempty"`
	Password    string `json:"password,omitempty"`
	RequestUser bool   `json:"requestUser,omitempty"`
}

func (p *payload) Send(url string) ([]byte, error) {
	b, err := json.Marshal(p)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "mim/1.0")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}
