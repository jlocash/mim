package minecraft

type Rule struct {
	Action string            `json:"action"`
	OS     map[string]string `json:"os"`
}
