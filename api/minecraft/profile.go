package minecraft

type Profile struct {
	Agent         string `json:"agent,omitempty"`
	ID            string `json:"id"`
	Name          string `json:"name"`
	UserID        string `json:"userId"`
	CreatedAt     int    `json:"createdAt"`
	LegacyProfile bool   `json:"legacyProfile"`
	Suspended     bool   `json:"suspended"`
	Paid          bool   `json:"paid"`
	Migrated      bool   `json:"migrated"`
	Legacy        bool   `json:"legacy"`
}
