package minecraft

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const VERSION_MANIFEST_URL = "https://launchermeta.mojang.com/mc/game/version_manifest.json"

type VersionManifest struct {
	Latest struct {
		Release  string `json:"release"`
		Snapshot string `json:"snapshot"`
	} `json:"latest"`
	Versions []VersionInfo `json:"versions"`
}

func GetVersionManifest() (VersionManifest, error) {
	var vm VersionManifest

	resp, err := http.Get(VERSION_MANIFEST_URL)
	if err != nil {
		return vm, err
	}

	defer resp.Body.Close()
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return vm, err
	}

	err = json.Unmarshal(bytes, &vm)
	return vm, err
}

func GetVersion(id string) (Version, error) {
	var v Version

	versions, err := ListVersions()
	if err != nil {
		return v, err
	}

	for _, info := range versions {
		if info.ID == id {
			return info.GetVersion()
		}
	}

	return v, fmt.Errorf("no version found for id: %s", id)
}

func GetLatestRelease() (VersionInfo, error) {
	var info VersionInfo

	manifest, err := GetVersionManifest()
	if err != nil {
		return info, err
	}

	id := manifest.Latest.Release
	for _, version := range manifest.Versions {
		if version.ID == id {
			info = version
		}
	}

	return info, nil
}

func GetLatestSnapshot() (VersionInfo, error) {
	var info VersionInfo

	manifest, err := GetVersionManifest()
	if err != nil {
		return info, err
	}

	id := manifest.Latest.Snapshot
	for _, version := range manifest.Versions {
		if version.ID == id {
			info = version
		}
	}

	return info, nil
}

func ListVersions() ([]VersionInfo, error) {
	manifest, err := GetVersionManifest()
	if err != nil {
		return nil, err
	}

	return manifest.Versions, nil
}
