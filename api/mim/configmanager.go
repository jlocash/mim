package mim

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path"
)

type ConfigManager struct {
	properties    map[string]*ConfigProperty
	BaseDirectory string
	initialized   bool
}

func NewConfigManager(base string) *ConfigManager {
	return &ConfigManager{
		properties:    make(map[string]*ConfigProperty),
		BaseDirectory: base,
	}
}

func (cm *ConfigManager) Get(name string) *ConfigProperty {
	property, exists := cm.properties[name]
	if !exists {
		return nil
	}

	return property
}

func (cm *ConfigManager) Init() error {
	config := path.Join(cm.BaseDirectory, "config.json")
	if _, err := os.Stat(config); os.IsNotExist(err) {
		return nil
	}

	bytes, err := ioutil.ReadFile(config)
	if err != nil {
		return err
	}

	if err = json.Unmarshal(bytes, &cm.properties); err != nil {
		return err
	}

	cm.initialized = true
	return nil
}

func (cm *ConfigManager) Close() error {
	if !cm.initialized {
		return nil
	}

	bytes, err := json.MarshalIndent(cm.properties, "", "    ")
	if err != nil {
		return err
	}

	if _, err = os.Stat(cm.BaseDirectory); os.IsNotExist(err) {
		if err = os.MkdirAll(cm.BaseDirectory, os.ModePerm); err != nil {
			return err
		}
	}

	config := path.Join(cm.BaseDirectory, "config.json")
	return ioutil.WriteFile(config, bytes, 0644)
}
