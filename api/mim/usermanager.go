package mim

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"gitlab.com/jlocash/mim/api/minecraft"
)

type UserManager struct {
	accounts      map[string]*minecraft.UserAccount
	BaseDirectory string
	initialized   bool
}

func NewUserManager(base string) *UserManager {
	return &UserManager{
		accounts:      make(map[string]*minecraft.UserAccount),
		BaseDirectory: path.Join(base, "accounts"),
	}
}

func (um *UserManager) Add(username string, password string) error {
	if _, ok := um.accounts[username]; ok {
		return fmt.Errorf("User already authenticated: %s", username)
	}

	user, err := minecraft.Authenticate(username, password)
	if err != nil {
		return err
	}

	um.accounts[username] = &user
	return nil
}

func (um *UserManager) Remove(username string) error {
	if _, ok := um.accounts[username]; !ok {
		return fmt.Errorf("user not found: %s", username)
	}

	if err := um.accounts[username].Invalidate(); err != nil {
		return err
	}

	delete(um.accounts, username)
	return nil
}

func (um *UserManager) Get(username string) *minecraft.UserAccount {
	account, exists := um.accounts[username]
	if !exists {
		return nil
	}

	return account
}

func (um *UserManager) Init() error {
	config := path.Join(um.BaseDirectory, "accounts.json")
	if _, err := os.Stat(config); os.IsNotExist(err) {
		return nil
	}

	bytes, err := ioutil.ReadFile(config)
	if err != nil {
		return err
	}

	if err = json.Unmarshal(bytes, &um.accounts); err != nil {
		return err
	}

	um.initialized = true
	return nil
}

func (um *UserManager) Close() error {
	bytes, err := json.MarshalIndent(um.accounts, "", "    ")
	if err != nil {
		return err
	}

	if _, err = os.Stat(um.BaseDirectory); os.IsNotExist(err) {
		if err = os.MkdirAll(um.BaseDirectory, os.ModePerm); err != nil {
			return err
		}
	}

	config := path.Join(um.BaseDirectory, "accounts.json")
	return ioutil.WriteFile(config, bytes, 0644)
}
