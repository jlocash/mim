package mim

type FileValidator interface {
	Verify() error
	Acquire() error
}

type FileValidatorGroup []FileValidator

func (fvg *FileValidatorGroup) VerifyAndAcquire() error {
	for _, fv := range *fvg {
		if err := fv.Verify(); err != nil {
			if err = fv.Acquire(); err != nil {
				return err
			}
		}
	}

	return nil
}
