module gitlab.com/jlocash/mim

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/spf13/cobra v0.0.5
	golang.org/x/crypto v0.0.0-20191206172530-e9b2fee46413
)
