package subcmd

import (
	"os/user"
	"path"

	"github.com/spf13/cobra"
	"gitlab.com/jlocash/mim/api/mim"
)

var (
	rootCmd = &cobra.Command{
		Use:   "mim",
		Short: "Minecraft Instance Manager",
	}
)

func initializeMimClient() (*mim.Client, error) {
	u, err := user.Current()
	if err != nil {
		return nil, err
	}

	client := mim.NewClient(path.Join(u.HomeDir, ".mim"))
	if err := client.Init(); err != nil {
		return nil, err
	}

	return client, nil
}

func Execute() {
	rootCmd.Execute()
}
