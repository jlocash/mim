package subcmd

import (
	"fmt"
	"log"
	"syscall"

	"github.com/spf13/cobra"
	"golang.org/x/crypto/ssh/terminal"
)

var (
	userCmd = &cobra.Command{
		Use:   "user",
		Short: "Mojang user account management",
	}

	userAddCmd = &cobra.Command{
		Use:   "add",
		Short: "Authenticate a new Mojang account",
		Run: func(command *cobra.Command, args []string) {
			client, err := initializeMimClient()
			if err != nil {
				log.Fatal(err)
			}

			defer client.Close()

			username, err := command.Flags().GetString("username")
			if err != nil {
				log.Fatal(err)
			}

			fmt.Printf("Mojang password (%s): ", username)
			password, err := terminal.ReadPassword(int(syscall.Stdin))
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println()

			if err = client.UserAccounts.Add(username, string(password)); err != nil {
				log.Fatal(err)
			}
		},
	}

	userRefreshTokenCmd = &cobra.Command{
		Use:   "refresh-token",
		Short: "Refresh the access token of a user account",
		Args:  cobra.ExactArgs(1),
		Run: func(command *cobra.Command, args []string) {
			client, err := initializeMimClient()
			if err != nil {
				log.Fatal(err)
			}

			defer client.Close()

			user, err := client.UserAccounts.Get(args[0])
			if err != nil {
				log.Fatal(err)
			}

			if err = user.Refresh(); err != nil {
				log.Fatal(err)
			}
		},
	}
)

func init() {
	userAddCmd.Flags().StringP("username", "u", "", "")
	userAddCmd.MarkFlagRequired("username")
	userCmd.AddCommand(userAddCmd)
	userCmd.AddCommand(userRefreshTokenCmd)
	rootCmd.AddCommand(userCmd)
}
