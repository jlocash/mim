package subcmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/jlocash/mim/api/minecraft"
)

var (
	instanceCmd = &cobra.Command{
		Use:   "instance",
		Short: "instance management",
	}

	instanceCreateCmd = &cobra.Command{
		Use:  "create",
		Args: cobra.ExactArgs(1),
		Run: func(command *cobra.Command, args []string) {
			client, err := initializeMimClient()
			if err != nil {
				log.Fatal(err)
			}

			defer client.Close()

			id, err := command.Flags().GetString("id")
			if err != nil {
				log.Fatal(err)
			}

			version, err := minecraft.GetVersion(id)
			if err != nil {
				log.Fatal(err)
			}

			name := args[0]
			if err = client.Instances.Add(version, name); err != nil {
				log.Fatal(err)
			}
		},
	}
)

func init() {
	instanceCreateCmd.Flags().String("id", "", "version ID to use (eg. 1.14.4)")
	instanceCmd.AddCommand(instanceCreateCmd)
	rootCmd.AddCommand(instanceCmd)
}
